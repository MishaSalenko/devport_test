@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @guest
                        @include('_register')
                    @else
                        Hello, {{ \Auth::user()->name }}
                        <br>
                        Maybe you want,
                        <form method="POST" action="{{ route('dynamic_page.generate') }}" class="mr-1">
                            @csrf
                            <button class="btn btn-bg btn-success"> Generate new page</button>
                        </form>
                    @endguest
                </div>
            </div>
        </div>
    </div>
@endsection
