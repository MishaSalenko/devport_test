@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <span>Dynamic page link: {{ $dynamicPage->slug }}</span>
                <hr>
                <form method="POST" action="{{ route('dynamic_page.generate') }}" class="mr-1">
                    @csrf
                    <button class="btn btn-bg btn-success"> Generate new page</button>
                </form>
                <hr>
                <form method="POST" action="{{ route('dynamic_page.inactive', $dynamicPage) }}">
                    @csrf
                    @method('PUT')
                    <button class="btn btn-bg btn-danger"> Inactive Dynamic Page</button>
                </form>
                <hr>
                <lucky-component></lucky-component>
                <hr>
                <history-component></history-component>
            </div>
        </div>
    </div>
@endsection
