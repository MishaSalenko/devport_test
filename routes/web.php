<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/dynamic-page/{dynamicPage}', 'DynamicPageController@view')->name('dynamic_page.view');
    Route::post('/dynamic-page/generate', 'DynamicPageController@generate')->name('dynamic_page.generate');
    Route::put('/dynamic-page/inactive/{dynamicPage}', 'DynamicPageController@inactive')->name('dynamic_page.inactive');

    Route::get('/feeling-lucky', 'FeelingLuckyController@index')->name('feeling_lucky.index');
    Route::get('/feeling-lucky/history', 'FeelingLuckyController@history')->name('feeling_lucky.history');

});
