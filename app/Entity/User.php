<?php

namespace App\Entity;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property string $phone
 */
class User extends Authenticatable
{
    use Notifiable;

    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    protected $fillable = [
        'name', 'phone', 'role'
    ];

    public static function register(string $name, string $phone): self
    {
        return static::create([
            'name' => $name,
            'phone' => $phone,
            'role' => self::ROLE_USER,
        ]);
    }
}
