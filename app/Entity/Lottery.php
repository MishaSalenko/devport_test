<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Lottery
 * @property int $id
 * @property string $victory_result
 * @property int $rand_number
 * @property int $user_id
 * @property float $amount
 *
 * @property User $user
 * @method Builder forUser(User $user)
 */
class Lottery extends Model
{
    public $timestamps = false;

    protected $table = 'lottery_histories';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }
}
