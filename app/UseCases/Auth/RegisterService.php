<?php

namespace App\UseCases\Auth;

use App\Entity\User;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\DB;

class RegisterService
{
    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function register(RegisterRequest $request): void
    {
        DB::transaction(function () use ($request) {

            $user = User::register(
                $request['name'],
                $request['phone']
            );

            $this->dispatcher->dispatch(new Registered($user));
            \Auth::login($user, false);
        });
    }
}
