<?php

namespace App\UseCases\Lottery;

use App\Entity\Lottery;
use App\Entity\User;
use Illuminate\Support\Facades\DB;

class LotteryService
{
    public function create(
        int $userId,
        int $randNumber,
        string $victoryResult,
        float $amount
    ): void
    {
        $user = $this->getUser($userId);

        DB::transaction(function () use ($user, $randNumber, $victoryResult, $amount) {

            /** @var Lottery $lottery */
            $lottery = Lottery::make([
                'victory_result' => $victoryResult,
                'rand_number' => $randNumber,
                'amount' => $amount,
            ]);

            $lottery->user()->associate($user);
            $lottery->saveOrFail();
        });
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function getUser(int $userId): ?User
    {
        return User::findOrFail($userId);
    }
}
