<?php

namespace App\UseCases\Page;

use App\Entity\DynamicPage;
use App\Entity\User;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class PageService
{
    public function create(int $userId, Carbon $date): DynamicPage
    {
        $user = $this->getUser($userId);

        return DB::transaction(function () use ($user, $date) {

            /** @var DynamicPage $page */
            $page = DynamicPage::make([
                'slug' => Uuid::uuid4(),
                'status' => DynamicPage::STATUS_ACTIVE,
                'expires_at' => $date->copy()->addDay(7),
            ]);

            $page->user()->associate($user);
            $page->saveOrFail();

            return $page;
        });
    }

    public function inActive(int $dynamicPageId): void
    {
        $dynamicPage = $this->getDynamicPage($dynamicPageId);
        $dynamicPage->update([
            'status' => DynamicPage::STATUS_INACTIVE,
        ]);
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function getUser(int $userId): ?User
    {
        return User::findOrFail($userId);
    }

    /**
     * @param int $dynamicPageId
     * @return DynamicPage|null
     */
    public function getDynamicPage(int $dynamicPageId): ?DynamicPage
    {
        return DynamicPage::findOrFail($dynamicPageId);
    }
}
