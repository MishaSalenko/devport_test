<?php

namespace App\Services\Lottery;

use App\Entity\Lottery;
use App\Entity\User;

class History
{
    private int $limit = 3;

    public function lastThreeFields(User $user)
    {
        return Lottery::forUser($user)
            ->select('victory_result', 'rand_number', 'amount')
            ->orderBy('id', 'desc')
            ->take($this->limit)
            ->get();
    }
}
