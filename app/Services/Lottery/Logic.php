<?php

namespace App\Services\Lottery;

class Logic
{
    public const WIN = 'WIN';
    public const LOSE = 'LOSE';

    private int $randNumber;

    public function __construct()
    {
        $this->randNumber = rand(1, 1000);
    }

    /**
     * @var array
     * Data for calculate the amount of the winner
     */
    private const DEPENDENCE = [
        [
            'sum' => 900,
            'percent' => 0.7,
        ],
        [
            'sum' => 600,
            'percent' => 0.5,
        ],
        [
            'sum' => 300,
            'percent' => 0.3,
        ],
        [
            'sum' => 0,
            'percent' => 0.1,
        ]
    ];

    public function handle(): array
    {
        return [
            'rand_number' => $this->randNumber,
            'victory_result' => $this->victoryResult($this->randNumber),
            'amount' => $this->winAmount($this->randNumber),
        ];
    }


    /**
     * @param int $number
     * @return string
     */
    private function victoryResult(int $number): string
    {
        if ($this->parityCheck($number)) {
            return self::WIN;
        }
        return self::LOSE;
    }

    /**
     * @param int $number
     * @return float
     */
    public function winAmount(int $number): float
    {
        $amount = 0;
        if ($this->parityCheck($number)) {
            $amount = $this->calcAmount($number);
        }
        return $amount;
    }

    /**
     * Looking for the closest number
     * to the "sum" field from the array,
     * but which is larger, and take the percentage
     * for calculation amount
     *
     * @param int $number
     * @return float
     */
    private function calcAmount(int $number): float
    {
        $differenceSum = [];
        foreach (self::DEPENDENCE as $percent) {
            if (($number - $percent['sum']) >= 0) {
                $differenceSum["{$percent['percent']}"] = $number - $percent['sum'];
            }
        }
        asort($differenceSum);
        $percent = array_key_first($differenceSum);

        return round($number * $percent, 2);
    }

    /**
     * @param int $number
     * @return bool
     */
    private function parityCheck(int $number): bool
    {
        if ($number % 2 === 0) {
            return true;
        }
        return false;
    }
}
