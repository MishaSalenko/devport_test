<?php

namespace App\Http\Controllers;

use App\Entity\Lottery;
use App\Services\Lottery\Logic as LotteryLogic;
use App\UseCases\Lottery\LotteryService as LotteryCreate;
use App\Services\Lottery\History as LotteryHistory;

class FeelingLuckyController extends Controller
{
    private LotteryLogic $serviceLogic;
    private LotteryCreate $serviceCreate;
    private LotteryHistory $serviceHistory;

    public function __construct(
        LotteryLogic $serviceLogic,
        LotteryCreate $serviceCreate,
        LotteryHistory $serviceHistory
    )
    {
        $this->serviceLogic = $serviceLogic;
        $this->serviceCreate = $serviceCreate;
        $this->serviceHistory = $serviceHistory;
    }

    public function index()
    {
        $data = $this->serviceLogic->handle();

        $this->serviceCreate->create(
            \Auth::user()->id,
            $data['rand_number'],
            $data['victory_result'],
            $data['amount']
        );

        return response()->json($data);
    }

    public function history()
    {
        return response()->json($this->serviceHistory->lastThreeFields(\Auth::user()));
    }
}
