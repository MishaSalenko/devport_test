<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\UseCases\Auth\RegisterService;
use App\UseCases\Page\PageService;
use Carbon\Carbon;

class RegisterController extends Controller
{
    private RegisterService $registerService;
    private PageService $pageService;

    public function __construct(RegisterService $registerService, PageService $pageService)
    {
        $this->middleware('guest');
        $this->registerService = $registerService;
        $this->pageService = $pageService;
    }

    public function register(RegisterRequest $request)
    {
        try {
            $this->registerService->register($request);
            $page = $this->pageService->create(\Auth::id(), Carbon::now());
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('dynamic_page.view',$page);
    }
}
