<?php

namespace App\Http\Controllers;

use App\Entity\DynamicPage;
use App\UseCases\Page\PageService;
use Carbon\Carbon;

class DynamicPageController extends Controller
{
    private PageService $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    public function view(DynamicPage $dynamicPage)
    {
        if (!$dynamicPage->isActive() || $dynamicPage->isExpired()) {
            abort(403);
        }
        return view('dynamic-page', compact('dynamicPage'));
    }

    public function generate()
    {
        try {
            $page = $this->pageService->create(\Auth::id(), Carbon::now());
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('dynamic_page.view', $page);
    }

    public function inActive(DynamicPage $dynamicPage)
    {
        try {
             $this->pageService->inActive($dynamicPage->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('home');
    }
}
